---
title: Anime
description: "Anime"
date: 2024-01-05T00:00:00.545Z
preview: ""
draft: true
tags: []
categories: []
---

Triés par ordre alphabétique, inclu des coups de coeur personnels, des classiques, des anime à succès, et d'autres que j'ai pu simplement prendre plaisir à regarder un jour. Animation japonaise ans l'immense majorité, mais quelques productions coréennes et américaines en reprenant les codes s'y trouvent aussi.

**********

#### A day before us
(연애하루전)

\\(#Romance #Shorts #SliceOfLife #SlowPace\\)

**********

#### Aggretsuko
(アグレッシブ烈子)

\\(#Comédie #SliceOfLive\\)

**********

#### Arte
(アルテ)

\\(#Historique #Peinture\\)

**********

#### Atarashii Joushi wa Dotennen
(新しい上司はど天然)
\\(#Comédie #Ikemen #SliceOfLife\\)

**********

#### Barakamon
(ばらかもん)
\\(#Calligraphie #SliceOfLife\\)

**********

#### Bartender - Glass of God
(バーテンダー 神のグラス)
\\(#Gastro #Drame #SliceOfLife\\)

**********

#### Boku dake ga inai machi
(僕だけがいない街)
\\(#Drame #Mystère #Seinen #VoyageTemporel\\)

**********

#### Bokura ha minna kawaisou
(僕らはみんな河合荘)
\\(#Romance\\)

**********

#### Boukensha ni Naritai to Miyako ni Deteitta Musume ga S-Rank ni Natteta
(冒険者になりたいと都に出て行った娘がＳランクになってた)
\\(#Aventure #Fantaisie\\)

**********

#### Byousoku 5cm
(秒速5センチメートル)
\\(#Romance #SliceOfLife #SlowPace\\)

**********

#### Chainsaw Man
(チェンソーマン)
\\(#Action #Fantastique #Seinen #SuperPouvoirs\\)

**********

#### Comic girls
(こみっくがーるず)
\\(#Comédie #Moe\\)

**********

#### Cool Doji Danshi
(クールドジ男子)
\\(#Comédie #Ikemen #SliceOfLife\\)

**********

#### Cowboy bebop
(カウボーイビバップ)
\\(#Action #Aventure #SciFi\\)

**********

#### Death note
(デスノート)
\\(#Détective #Fantastique #Thriller\\)

**********

#### Doukyonin ha hiza tokidoki atama no ue
(同居人はひざ、時々、頭のうえ。)
\\(#Chat #Comédie #SliceOfLife\\)

**********

#### Dungeon meshi
(ダンジョン飯)
\\(#Aventure #Comédie #Cuisine #Fantaisie #RPG\\)

**********

#### Eizouken ni wa Te wo Dasu na!
(映像研には手を出すな!)
\\(#Animation #Club #Comédie\\)

**********

#### Elfen lied
(エルフェンリート)
\\(#Action #Fantastique #Horreur\\)

**********

#### Enen no Shouboutai
(炎炎ノ消防隊)
\\(#Action #Fantastique #SuperPouvoirs\\)

**********

#### Full Metal Alchemist: brotherhood
(鋼の錬金術師 BROTHERHOOD)
\\(#Aventure #Fantaisie #SuperPouvoirs\\)

**********

#### Fumetsu no anata e
(不滅のあなたへ)
\\(#Drame #Fantaisie #Mystère\\)

**********

#### Gakkougurashi
(がっこうぐらし!)
\\(#Horreur #Moe #Seinen #Zombies\\)

**********

#### Gekkan shoujo nozakikun
(月刊少女野崎くん)
\\(#Comédie #Mangaka #Romance\\)

**********

#### Giji-Harem
(疑似ハーレム)
\\(#Club #Comédie #Romance\\)

**********

#### Gin no saji
(銀の匙)
\\(#Club #Comédie #LycéeAgricole #Romance\\)

**********

#### Goblin Slayer
(ゴブリンスレイヤー)
\\(#Action #Aventure #Fantaisie #Horreur #RPG\\)

**********

#### Gokushufudou
(極主夫道)
\\(#Comédie #Cuisine #Yakuza\\)

**********

#### Golden Boy
(ゴールデンボーイ)
\\(#Comédie #Ecchi\\)

**********

#### Golden Kamuy
(ゴールデンカムイ)
\\(#Action #Aventure #Drame #Historique\\)

**********

#### Goukon ni ittara onna ga inakatta hanashi
(合コンに行ったら女がいなかった話)
\\(#Comédie #Romance\\)

**********

#### Hai to gensou no grimgar
(灰と幻想のグリムガル)
\\(#Action #Aventure #Drame #Fantaisie #Isekai #RPG\\)

**********

#### Hakumei to mikochi
(ハクメイとミコチ)
\\(#Fantaisie #SliceOfLife #SlowPace\\)

**********

#### Honzuki no Gekokujou
(本好きの下剋上 · 司書になるためには手段を選んでいられません)
\\(#Fantaisie #Isekai\\)

**********

#### Horimiya
(ホリミヤ)
\\(#Comédie #Romance #SliceOfLife\\)

**********

#### Hotaru no mori e
(蛍火の杜へ)
\\(#Drame #Fantastique #LongMétrage #Romance\\)

**********

#### Housekishou Richard-shi no Nazo Kantei
(宝石商リチャード氏の謎鑑定)
\\(#Détective #Ikemen #SliceOfLife\\)

**********

#### Hyouge Mono
(へうげもの)
\\(#Comédie #Drame #Historique #Samurai\\)

**********

#### Hyouka
(氷菓)
\\(#Drame #Mystère #Romance\\)

**********

#### Isekai Izakaya
(異世界居酒屋～古都アイテーリアの居酒屋のぶ～)
\\(#Comédie #Cuisine #Fantaisie #Isekai #SliceOfLife\\)

**********

#### Isekai Shokudou
(異世界食堂)
\\(#Comédie #Cuisine #Fantaisie #Isekai\\)

**********

#### Kaguya hime no monogatari
(かぐや姫の物語)
\\(#Fantastique #Historique #LongMétrage\\)

**********

#### Kaguya-sama
(かぐや様は告らせたい ～天才たちの恋愛頭脳戦～)
\\(#Comédie #Romance\\)

**********

#### Kaijuu 8-gou
(怪獣8号)
\\(#Action #Fantastique #Kaijuu\\)

**********

#### Kaizoku oujo
(海賊王女)
\\(#Aventure #Drame #Fantastique #Mystère #Pirate\\)

**********

#### Kakushigoto
(かくしごと)
\\(#Comédie #SliceOfLife\\)

**********

#### Kanojo ga Koushakutei ni Itta Riyuu
(彼女が公爵邸に行った理由)
\\(#Fantaisie #Isekai #Mystère #Romance #Shoujo\\)

**********

#### Kimetsu no yaiba
(鬼滅の刃)
\\(#Action #Aventure #Fantastique #Shounen\\)

**********

#### Kimi no Suizou wo Tabetai
(君の膵臓をたべたい)
\\(#Drame #LongMétrage #Maladie #Romance\\)

**********

#### Kimi wa Houkago Insomnia
(君は放課後インソムニア)
\\(#Astrophotographie #Club #Romance #SliceOfLife\\)

**********

#### Kitsutsuki tantei dokoro
(啄木鳥探偵處)
\\(#Détective #Historique\\)

**********

#### Koe no katachi
(聲の形)
\\(#Drame #Handicap #Romance\\)

**********

#### Koori Zokusei Danshi to Cool na Douryou Joshi
(氷属性男子とクールな同僚女子)
\\(#Comédie #Romance #Fantastique\\)

**********

#### Kotonoha no niwa
(言の葉の庭)
\\(#Drame #Romance #SliceOfLife\\)

**********

#### Kumichou Musume to Sewagakari
(組長娘と世話係)
\\(#Comédie #Yakuza\\)

**********

#### Kusuriya no Hitorigoto
(薬屋のひとりごと)
\\(#Détective #Historique #Médecine #Mystère #Romance\\)

**********

#### Log horizon
(ログ・ホライズン)
\\(#Aventure #Fantaisie #Isekai #Romance #RPG\\)

**********

#### Made in Abyss
(メイドインアビス)
\\(#Aventure #Drame #Fantaisie #Mystère\\)

**********

#### Maiko-san Chi no Makanai-san
()
\\(#\\)

**********

#### Majo no tabitabi
()
\\(\\)

**********

#### Maoujou de Oyasumi
()
\\(#\\)

**********

#### Mashle
()
\\(#\\)

**********

#### Mononoke
()
\\(#\\)

**********

#### Mushishi
()
\\(#Fansaty #Isekai #SliceOfLife\\)

**********

#### Nami yo Kiitekure
()
\\(#\\)

**********

#### Natsuyuki rendez-vous
()
\\(#\\)

**********

#### Nige Jouzu no Wakagimi
(逃げ上手の若君)
\\(#Action #Fantastique #PseudoHistorique\\)

**********

#### One punch man
()
\\(#\\)

**********

#### Ookami to koushinryou: Merchant meets the wise wolf
(狼と香辛料 merchant meets the wise wolf)
\\(#Aventure #Fantaisie #Romance\\)

**********

#### Oosama ranking
()
\\(#\\)

**********

#### Otonari ni Ginga
()
\\(#\\)

**********

#### Overlord
()
\\(#Fantaisie #Isekai #Romance\\)

**********

#### Piano no Mori
()
\\(#\\)

**********

#### Ping pong
()
\\(#\\)

**********

#### Ranway de waratte
()
\\(#\\)

**********

#### Saihate no Paladin
()
\\(#\\)

**********

#### Sakurako-san no ashimoto ni wa shitai ga umatteiru
()
\\(#\\)

**********

#### Sangatsu no lion
()
\\(#\\)

**********

#### Saraiya Goyou
()
\\(#\\)

**********

#### Seijo no Maryoku wa Bannou desu
(聖女の魔力は万能です)
\\(#Fantaisie #Isekai #Romance\\)

**********

#### Seitokai yakuindomo
()
\\(#\\)

**********

#### Senryuu shoujo
()
\\(#\\)

**********

#### Shigatsu ha kimi no uso
()
\\(#\\)

**********

#### Shinsekai yori
()
\\(#Fansaty #Isekai #SliceOfLife\\)

**********

#### Shokugeki no souma
()
\\(#\\)

**********

#### Skip to Loafer
()
\\(#\\)

**********

#### Sono bisque doll wa koi wo suru
()
\\(\\)

**********

#### Soredemo Ayumu wa Yosetekuru
()
\\(#Romance #Comédie\\)

**********

#### Sousou no Frieren
()
\\(#\\)

**********

#### SPY FAMILY
()
\\(#\\)

**********

#### Sword Art Online
(ソードアート・オンライン)
\\(#Action #Fantaisie #RPG\\)

**********

#### Tada kun wa koi ga shinai
()
\\(#\\)

**********

#### Tensei shitara ken deshita
()
\\(#\\)

**********

#### Tensei shitara slime datta ken
()
\\(#Fantaisie #Isekai #Romance\\)

**********

#### Toki wo kakeru shoujo
()
\\(#\\)

**********

#### Tondemo Skill de Isekai Hourou Meshi
()
\\(#\\)

**********

#### Tsurune: Kazemai Koukou Kyuudoubu
()
\\(#\\)

**********

#### Uchi no Shishou wa Shippo ga Nai
()
\\(#\\)

**********

#### Usagi drop
()
\\(#\\)

**********

#### Vinland Saga
()
\\(#\\)

**********

#### Violet Evergarden
()
\\(#\\)

**********

#### Wotaku ni Koi wa Muzukashii
()
\\(#\\)

**********

#### Yagate kimi ni naru
()
\\(#\\)

**********

#### Yakusoku no neverland
()
\\(#\\)

**********

#### Yamada-kun to Lv999 no Koi wo Suru
()
\\(#\\)

**********

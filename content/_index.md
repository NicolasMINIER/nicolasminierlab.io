---
title: Présentation
description: ""
date: 2023-12-01T00:00:00.545Z
preview: ""
draft: false
tags: []
categories: []
---

Docteur en biologie et épidémiologiste de formation, je travaille dans un laboratoire d'épidémiologie des rayonnements ionisants, ce qui m'amène notamment à travailler sur le Système National de Données de Santé (SNDS) qui rassemble les données de santé de plus de 60 millions de bénéficiaires de l'asurance maladie. Sur mon temps libre, j'ai à coeur de vulgariser quelques aspects de mon travail, ainsi que de partager quelques réflexions, ainsi que réponses aux questions que j'ai pu me poser et sur lesquelles les sciences du vivant ont quelque chose à dire.

Vous trouverez rassemblés [ici](travaux-vulgarises/) des formats accessibles des travaux de recherche auxquels j'ai pu participer. Par [là](billets/), vous pourrez voir d'autres billets traitant de biologie, santé publique, épidémiologie, ainsi que ce que j'avais commencé à vulgariser dans une vie antérieure, lorsque j'étais en master d'ingénierie biomédicale. Enfin, vous trouverez [ici](toolbox/) des ressources de biostatistiques, *machine learning*, épidémiologie, *etc*.

À côté de ça, je suis trésorier du [Café des Sciences](https://www.cafe-sciences.org/), et je vulgarise de temps à autres, initialement sur [youtube](https://www.youtube.com/@Nicolas_DataBio), aujourd'hui sans doute plutôt sur ce site (voir l'onglet DataBio). Je suis également *Maître du Jeu* pour un groupe de joueurs et joueuses de *Donjons et Dragons* et, si l'envie vous prend, vous pouvez vous perdre dans mes notes et les récits d'aventure du groupe [ici](donjons-et-dragons/). Enfin, vous trouverez [ici](memos/) quelques billets à but d'aide mémoire, sur des sujets pour lesquels je me suis pris d'intérêt un jour.


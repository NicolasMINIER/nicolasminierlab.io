---
title: À propos du site
description: ""
date: 2023-12-01T00:00:00.545Z
preview: ""
draft: false
tags: []
categories: []
---

Ce site personnel se veut un agrégat de beaucoup de choses que je souhaite garder sous la main, pour un usage personnel, ou pour le partager avec d'autres. Il peut s'agir de communiquer sur mes projets professionnels en épidémiologie, de vulgariser un sujet qui me tient à coeur, de tenir à jour mes notes de jeu de rôle, ou encore de pouvoir retourner quand l'envie me prend à des notes sur des sujets d'intérêt.

Ce site me sert aussi à m'exercer sur markdown, CI/CD, *etc.* En l'occurrence, ce site est assemblé par [Hugo](https://gohugo.io) sur la base du thème [Beautiful Hugo](https://github.com/halogenica/beautifulhugo), que j'ai adapté pour mes besoins personnels.



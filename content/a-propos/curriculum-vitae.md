---
title: Curriculum Vitae
description: ""
date: 2023-12-02T00:00:00.545Z
preview: ""
draft: false
tags: []
categories: []
---

Epidémiologiste de formation, j'ai travaillé à plusieurs reprise au développement d'algorithmes diagnostiques, au sein d’un consortium international (HEPSANET) intéressé par des outils adaptés aux contextes régionaux d’Afrique sub-Saharienne, notamment dans le traitement de l’hépatite B, et aujourd'hui à Santé Publique France où je développe des outils de suivi des AVC en France. Docteur en biologie (diplômé en 2021) avec trois ans d’expérience en recherche au sein de l’Institut Pasteur (Paris), je souhaite mettre cette expertise au service de l’interprétation des données de santé. Expériences en formation (premiers secours à la Croix-Rouge) et vulgarisation scientifique (biologie, crise COVID-19).

## Expérience

* **Biostatisticien** *(2024-2025)*. Je rejoins le laboratoire d'épidémiologie des rayonnements ionisants (LEPID) de l'Institut de Radioprotection et de Sûreté Nucléaire (IRSN, aujourd'hui ASNR) afin de mener des travaux sur la survenue de cancers radio-induits. Notamment, les cancers du sein et du poumon survenant suite à une exposition médicale aux rayonnements ionisants du fait de dépistage.

* **Chargé d'études scientifiques en santé publique / Biostatisticien** *(2023-2024)*. Au sein de l'Agence Nationale de Santé Publique (ANSP, plus connue sous le nom de "Santé Publique France"), je travaille au suivi épidémiologique des AVC en France. Plus précisément, je travaille sur les données du Système National de Données de Santé (SNDS), qui compile les consommations de soin (hospitalisation, consultations, consommation de produits de santé) de près de 60 millions de français. Grâce à cette base de données, ainsi qu'aux données du registre des AVC du Pays de Brest, je cherche à produire des métriques de la sévérité des AVC hospitalisés.

* **Epidémiologiste** *(2022) (stagiaire)*. Au sein de l'Unité d'Epidémiologie des Maladies Emergentes de l'Institut Pasteur (Paris), et en collaboration avec le consortium [HEPSANET](https://www.hepsanet.org), j'ai développé et validé un score simplifié (article publié [ici](https://doi.org/10.1016/S2468-1253(23)00449-1)), basé sur des biomarqueurs facile d’accès, qui permet de prédire de manière fiable l’éligibilité au traitement selon un standard clinique de référence (recommandations EASL 2017). Ce test est accessible dans des hôpitaux de district, contrairement aux outils de référence (biopsie, élastographie transitoire, qPCR, etc.), ouvrant la voie à une accélération de la mise sous traitement de nombreux patients éligibles dans des contextes d’accès aux ressource limité en Afrique sub-Saharienne.

* **Chercheur junior en biologie** *(2018-2021)*. J'ai réalisé mes travaux de thèse de bioingénierie (défendue auprès de l'Université de Technologies de Compiègne) au sein de l'unité Biomatériaux et Microfluidique de l'Institut Pasteur (Paris). Là, j'ai participé au développement d'un *microphysiological system* (plus communément appelé *organ-on-chip*/organe sur puce) miment le micro-environnement tissulaire de la paroi intestinal en détail : propriété élastiques, chimiques, étirement, ... De tels outils sont d'une grande utilité pour mener des expérience de biologie fondamentale (observer au microscope une infection bactérienne) comme de recherche précliniques (en disposant d'outils biologiques complexes pouvant parfois se substituer ou du moins réduire notre recours aux animaux de laboratoire)

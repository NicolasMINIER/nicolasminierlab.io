---
title: Interventions, participations, publications
description: ""
date: 2023-12-03T00:00:00.545Z
preview: ""
draft: false
tags: []
categories: []
---

Vous trouverez ici une sélection non-exhaustive de mes interventions et collaborations, médiatiques, scientifiques, ou avec des collègues du monde de la vulgarisation/médiation. Egalement, une sélection d'articles scientifiques qui illustre mes travaux.

## COVID-19

* [***Espace-Temps : Coronavirus***](https://www.plumfm.net/espacetemps) (Plum'FM, 6 février 2021)

* [***La pandémie de Covid-19 avait-elle été annoncée dans une série coréenne en 2018 ?***](/archives/2020-04-19-Journalistes-Solidaires-La-pandémie-de-Covid-19-avait-elle-été-annoncée-dans-une-série-coréenne-en-2018/) (Journalistes Solidaires, 19 avril 2020).

* [***KezaCovid19***](https://kezacovid19.wordpress.com/). Modérateur du serveur discord KezaCovid19, visant d'une part à mettre en relation scientifiques et journalistes, d'autre part à faire collaborer scientifiques et graphistes dans la production d'infographies mises en ligne.


## Articles scientifiques

* [**Development and evaluation of a simple treatment eligibility score (HEPSANET) to decentralise hepatitis B care in Africa: a cross-sectional study**](https://doi.org/10.1016/S2468-1253(23)00449-1), *The Lancet Gastroentero. & Hepato.*, 2024. Développement et validation d'un score simplifié destiné à identifier les personnes vivant avec le virus de l'hépatite B qui sont éligibles aux traitement antiviraux, dans des contextes d'accès aux ressources limité en Afrique sub-Saharienne.
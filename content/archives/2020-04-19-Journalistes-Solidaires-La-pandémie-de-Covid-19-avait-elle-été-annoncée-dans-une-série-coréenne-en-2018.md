---
title: Archives
description: "Reproduction ici de choses perdues"
date: 2025-02-13T00:00:00.545Z
preview: ""
draft: false
tags: []
categories: []
---

Je reproduis ici un article paru sur le site *Journalistes Solidaires* (initiative qui n'est plus active)

# La pandémie de Covid-19 avait-elle été annoncée dans une série coréenne en 2018 ?
Relecture par Alice Carel, correction par Anne Smadja, coordonné par Lina Fourneau, pour *Journalistes Solidaires*

C’est une autre vidéo, authentique cette fois, qui est devenue virale sur les réseaux sociaux. Dans une démonstration attestée « très simple à reproduire chez soi », des internautes montrent que l’épidémie de Covid-19 avait été annoncée il y a deux ans dans une série télévisée coréenne. L’extrait en question provient de l’épisode 10 de la première saison de la série coréenne *My Secret Terrius*, disponible sur Netflix et diffusée en septembre 2018. À partir de la 53e minute, différents arguments sont avancés et semblent troubler les spectateurs qui relaient cette vidéo : *« c’est un coronavirus mutant »*, *« il attaque le système respiratoire »*, *« c’est une attaque terroriste biochimique »*, *etc* … L’effet recherché est réussi puisque la fiction vient se frotter à la réalité que le monde est en train de traverser : celle de la crise sanitaire provoquée par la pandémie de Covid-19. Et si ces arguments avancés dans la série relèvent de la fiction, il n’en reste pas moins qu’ils trouvent un écho dans les théories complotistes et sont souvent brandis comme des vérités.

## Accroche pseudo scientifique 

Julien Giry, docteur en sciences politiques à l’université de Rennes 1 est spécialisé dans les théories du complot et le conspirationnisme. Concernant les prétendues prévisions, il explique que : *« le seul argument est de dire que l’épidémie était prévue à l’avance, parce que la série date de 2018. Toutes les hypothèses reposent là-dessus. En revanche, une simple recherche sur Internet rappelle que la découverte du coronavirus date des années 1960. Là, ce qui est surtout frappant, c’est le côté vraiment amateur de la vidéo. […] Si cette vidéo demande très peu de vérification, elle a pourtant pu devenir très virale, voire être imaginée comme une vérité. »*

Une vidéo parmi d’autres, issue d’une série qui tire ses arguments d’un fond prétendument scientifique. Pour bien comprendre les raisons pour lesquelles ces théories sont farfelues, Journalistes Solidaires a fait appel à Nicolas Minier, doctorant en bio-ingénierie et membre du Café des sciences. Il analyse, point par point, les extraits présentés :

1. C’est un coronavirus mutant
*« Ici, on est bien embêté par le terme vague de "mutant", qui est très employé dans la fiction alors qu'en biologie, son seul emploi ne nous apprend pas grand chose. Tous les virus présentent des mutations par rapport à leurs ancêtres, de même que vous et moi. »*

2. C'est de la même famille génétique que le MERS, SARS, et la grippe
*« Oui et non. Le terme "coronavirus" peut effectivement faire référence à une famille phylogénétique, celle des Coronaviridae. Dans cette famille, on retrouve bien les virus responsables des épidémies de SARS et de MERS, mais également d'une part non négligeable des "états grippaux" hivernaux (la crève, quoi). Cependant, dire  qu'il s'agit de la même famille que la grippe est faux, puisque les virus responsables de celle-ci appartiennent à la famille des Orthomyxoviridae. »*

3. Il attaque le système respiratoire
*« Vrai »*

4. Il a été modifié pour augmenter le taux de mortalité à 90 %
*« La séquence génétique du SARS-CoV-2 laisse clairement penser qu'il a évolué naturellement et qu'il n'est pas le fruit d'une sélection artificielle ni d'une manipulation par génie génétique. »* Le virus n’a donc pas été modifié et Nicolas Minier le confirme : *« Le taux de mortalité du SARS-CoV-2 n'est absolument pas de 90% ! »*

5. Le virus a une période incubation de deux à quatorze jours
*« Le SARS-CoV-2 a une incubation qui dure typiquement autour de cinq jours. Des extrêmes existent, mais restent des extrêmes. On considère que ceux-ci vont de deux à douze jours. »*

6. Le virus été modifié pour attaquer les poumons cinq minutes après l’exposition 
*« Cela va dépendre de ce que l'on entend par "attaquer les poumons"... S'y retrouver ? Dans ce cas, c'est immédiat, l'infection commence sitôt que le virus est inhalé. Causer des dégâts ? À priori, non. Ce serait d'ailleurs incompatible avec une période d'incubation de plusieurs jours. Il y a là une grosse incohérence. »*

7. Il n'existe aucun remède ou vaccin pour l’instant
*« Il n'existe aujourd'hui aucun remède spécifique au Covid-19, ni de vaccin. Cela ne veut pas dire que nous sommes impuissants, mais que nos traitements sont principalement symptomatiques, pour le moment. »*

8. C'est une attaque terroriste biochimique 
« Utiliser un virus modifié pour mener une action terroriste est extrêmement risqué ! Particulièrement si vous décidez d'avoir un virus avec une certaine période d'incubation, une contagiosité arrivant avant les symptômes, et une faible mortalité, c'est un cocktail parfait pour que cela se retourne contre vous. »*

Concernant le SARS-CoV-2, le virus n’a pas été créé en laboratoire (comme le rappelle le quatrième argument), il ne peut donc pas émaner d’une volonté humaine d’attaquer le monde. Ce mélange d’arguments vrais, plausibles et faux est une stratégie pour attirer le téléspectateur. Selon le chercheur en sciences politiques, Julien Giry, *« l’inexactitude factuelle n’est pas spécialement un problème parce que celui qui est déjà convaincu de toute façon sera convaincu et passera outre. Si vous voulez que ça soit crédible, une bonne théorie du complot doit toujours sembler plausible. Il faut que ça prenne une apparence scientifique, que ça fasse sérieux, même si ça ne l’est pas. L'important tient dans la crédibilité. »*

## L'ancrage dans le réel crédibilise un scénario 

Au-delà des arguments scientifiques, Nicolas Minier met en avant le fait que ces séries reprennent des thématiques scientifiques ancrées dans une réalité pour accrocher l’auditoire. Il imagine que, pour scénariser une thématique bioterroriste, le processus serait le suivant : *« […] Ancrer cela dans une peur passée, reprendre de vraies infos là où les choses ne sont pas dangereuses pour rendre crédible, gonfler les chiffres où l'on comprend qu'il y a du danger. C'est exactement ce qu'il se passe ici. [...] La durée d'incubation semble coller à la perfection, et ce serait une curieuse coïncidence... si ce n'étaient pas aussi les chiffres Wikipédia pour le SARS de 2003 ! La référence aux autres virus respiratoires participe également à cet “ancrage dans le réel”. Le terme de "mortalité "étant plus chargé que celui d'“incubation”, on le gonfle à l'extrême ! Clairement, la crédibilité scientifique n'est pas la première préoccupation de la série. »*

De son côté, Julien Giry étaie ce raisonnement en expliquant pourquoi cette scénarisation rencontre un tel succès : *« Dans le cas des théories liées au coronavirus, très souvent, un scénario de film ou une explication donnée dans une série semble familière au public, parce qu’il les a déjà entendus quelque part. Cela fait partie des scénarios plausibles dans son imaginaire, parce qu'il les a déjà vus dans les supports de culture populaire et qu’ils vont se calquer sur la réalité. »*

Sauf qu’il existe un point de bascule : le moment où une fiction se mélange avec la réalité en prenant la forme de théories. Pour Julien Giry, ce procédé tient à un souhait de se faire une raison : *« Lors d’une catastrophe comme celle-ci, on ressent un besoin d’explications. Mais, si celles données par les autorités sont souvent longues à venir, […] on trouve une alternative. Alors la théorie du complot tient à remplir ce vide, ce besoin d’explication en arguant que c’est arrivé “parce que” et "pour telle raison”. Le raisonnement conspirationniste amène la remise en cause de la version communément établie au nom d’une hypothèse jamais prouvée ni démontrée d’un complot. »*

À cela s’ajoute le contexte tout particulier de la crise sanitaire, *« dans un écosystème avec énormément de fausses informations »* Les fomenteurs de théories complotistes vont aussi chercher à attirer leur public en leur faisant partager le sentiment « d’avoir découvert le pot aux roses ». Dès lors, il paraît aisé de faire croire que la pandémie a été annoncée dans une série télévisée coréenne, d’autant plus qu’elle reprend des thématiques scientifiques existantes.

D’ailleurs, de nombreuses fake news jouent sur les mots : le coronavirus étant une famille de virus, ce terme a de nombreuses fois été associé exclusivement à la pandémie de Covid-19. Il s’agit donc de rester vigilant face aux dénominations employées.

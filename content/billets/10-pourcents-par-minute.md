---
title: 10 pourcents par minute
description: "La difficile balance entre exactitude et efficacité d’un message s’appuyant sur de la science"
date: 2023-12-04T00:00:00.545Z
preview: ""
draft: false
tags: [secourisme]
categories: []
---

![Thumbnail](/billets/10pc-par-minute/thumbnail.png "détournement de l'affiche du film d'animation '5cm per second', titrant ici '10 pourcents par minute'")

Dix pourcents par minute. C’est la vitesse à laquelle nos chances de survie s’amenuisent après un arrêt cardiaque si personne ne nous porte assistance. Ce chiffre, vous l’avez sans doute vu et revu, entendu et ré-entendu, je l’ai moi-même enseigné à des gens lorsque je les formais aux premiers secours, et pourtant ce chiffre est faux. Dans ce billet, posons-nous 2 questions. Premièrement, en quoi ce chiffre est-il faux ? Quel serait alors le véritable chiffre à placer sur cette vitesse à laquelle nos chances s’amenuisent ? Ensuite, pourquoi apprend-on ce chiffre de 10%, s’il est faux ? Quel peut être l’intérêt d’apprendre ce chiffre s’il ne correspond pas aux conclusions des études scientifiques menées sur le sujet ?

*****

### Ce qu'on lit et entend partout

Alors pour être plus précis et moins catégorique, ce chiffre, je ne l’ai retrouvé dans aucune conclusion d’article scientifique, et lorsqu’il est avancé dans un article de journal, dans une brochure, ou même dans les manuels de secourisme, il n’est généralement pas sourcé. Pour l'exemple, vous trouverez ici quelques liens vers des articles illustrant cet état de fait, dans [Le Monde](https://www.lemonde.fr/sciences/article/2018/05/30/arret-cardiaque-on-peut-sauver-chaque-annee-5-000-vies-supplementaires-en-france_5307029_1650684.html), [Le Figaro](http://sante.lefigaro.fr/actualite/2015/08/05/23998-crise-cardiaque-surtout-ne-pas-avoir-peur-dintervenir), sur [AlloDocteur](https://www.allodocteurs.fr/actualite-sante-peut-on-survivre-apres-minutes-d-arret-cardiaque-_14818.html), et jusque sur le site d'associations pourtant spécialisées comme [CardiaScience](https://www.cardiacscience.fr/arr%C3%AAt-cardiaque-soudain/) ou l'[*American Heart Association*](https://www.sciencedaily.com/releases/2018/02/180226085812.htm). On trouve parfois une variante, "7 à 10%", comme [ici](https://api.zoll.com/-/media/uploadedfiles/public_site/core_technologies/real_cpr_help/cpr-fakten-pdf.ashx) pour l'*American Heart Association* ou [là](http://www.cfrc.fr/documents/PSE1.pdf) pour consulter le référentiel sur la base duquel son formés les secouristes et pompiers français. Ce chiffre de 10% par minute, on le trouve même dans des cours de cardiologie mis en ligne par la Société Française de Cardiologie, [ici](http://www.sfcardio.fr/sites/default/files/pdf/Vivien.pdf)

Par contre trouver une publication qui avance ce chiffre **ET** vous renvoie vers une source scientifique, il va vous falloir farfouiller quelques temps sur internet pour trouver par exemple [cette revue de 2007](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2600120/) ou bien cette [brochure/mémo](https://www.heart.org/idc/groups/heart-public/@wcm/@adv/documents/downloadable/ucm_301646.pdf) de l’*American Heart Association* qui prend la peine cette fois-ci de citer une source scientifique. Dans les deux cas, on est renvoyé vers [cet article de 1993](https://pubmed.ncbi.nlm.nih.gov/8214853/). Bon, 1993, ça date un peu, mais je pense qu’on peut se permettre de penser que les humains des années 90 et ceux des années 2020 meurent à peu près à la même vitesse d’un arrêt cardiaque. Et puis de toute façon, c’est tout ce qu’on à se mettre sous la dent. Regardons donc ce qu’on y trouve.

L'approche de Mary Larsen et de ses collègues a été de sélectionner 1667 cas d’arrêts cardiaques "rattrapables". Je mets des guillements parce que oui, y’a aussi des cas où le cœur s’arrête parce que vous mourrez, auquel cas vous ne perdez pas à chaque minute ni 1 ni 7 ni 10% de chance de survie, vous êtes juste… mort. Dis autrement, ils essaient de sélectionner des arrêts cardiaques dont le décès n'est pas courru d'avance, typiquement des patients connus pour leur antécédents cardiovasculaires, dont la prise en charge révèle qu’ils étaient en fibrillation ventriculaire. Puis, ils se sont intéressé à ceux pour lesquels on dispose d’une chronologie précise des évènements.

Notamment, l'équipe de recherche s’intéresse au lapse de temps séparant la survenue de l’arrêt cardiaque de la réanimation par un témoin sans matériel, puis la pose d’un défibrillateur, puis la prise en charge complète par une équipe de secours. En comparant la survie ou non de la victime à tous ces chiffres, on peut estimer à quelle vitesse les chances de survie diminuent à chacune de ces étapes.

*****

### La vérité-vraie (TM)

Et alors vient le moment que vous attendez tous : quelles sont leurs conclusions ?

Tout d'abord, leur modèle fait commencer les chances de survies autour de 67%. Ce qui au final est assez logique, lorsque vous faites un arrêt, c’est qu’il s’est passé quelque chose de grave, vous ne commencez pas à 100% de chances de survie. En l’occurrence, si vous faites un arrêt cardiaque, même dans un hôpital à 2 mètre d’une équipe formée et équipée, vous avez 2 chances sur 3 de survivre, et 1 chance sur 3 d’y passer malgré tout. Puis, la personne en arrêt cardiaque perd, pour chaque minute qui passe :

 - 2,3% de chance de survie sans massage cardiaque,
 - 1,1% supplémentaire sans défibrillation,
 - 2,1% supplémentaire sans prise en charge complète.

Ce qui nous fait 5,5% de chance de survie en moins chaque minute si rien n’est fait. ***Avoir une réanimation, même partielle avec juste un massage cardiaque, ralentit cette baisse et offre des chances de survie supplémentaires.*** Vous trouverez ci-dessous deux schémas résumant le modèle développé par Larsen et collègues.

![Modèle-graphique-1](/billets/10pc-par-minute/ModelGraphique_1.png)

![Modèle-graphique-2](/billets/10pc-par-minute/ModelGraphique_2.png)

Donc si on se permettait de corriger ces 10% qu’on entend partout, on donnerait cette équation : \\(C(\\%) = 67 – 5,5 \times t\\). \\(C\\) correspondant aux chances de survie de la personne en arrêt cardiaque, exprimé en %, et \\(t\\) au temps écoulé depuis le début de l'arrêt cardiaque, en minutes.

Donc à aucun moment on ne trouve 10%, ni 7%, ni un chiffre entre les deux. Le seul chiffre que j’ai trouvé dans la littérature, et le seul auquel font références les articles et brochures lorsqu’elles prennent la peine de sourcer leur affirmation, c’est 5,5%.

Alors pourquoi voit-on 10% partout ? Pourquoi des fois 7 à 10% ? À mon avis, ça tient à une chose : dans le contexte de l’apprentissage des premiers secours, rien à fiche de la vérité, tant que l’on retient quelque chose d’utile. Ici, c’est de savoir que tout se joue dans les premières minutes, et que passé une douzaine de minutes, les chances de s’en sortir vivant sont très minces ! Elles existent, mais elles sont minces. Le message que l’on souhaite faire passer, quand on forme des gens aux premiers secours, ça n’est pas le chiffre exact de 5,5%, 7%, ou 10%. Le message que l’on souhaite faire passer, c’est qu’***il ne faut pas attendre ni les pompiers ni le SMUR ni d’être à l’hôpital pour commencer un massage cardiaque : il faut s’y mettre aussi tôt que possible.*** Les chances sont faibles passé 10-12 minutes, donc on préfère arrondir à 10% au lieu d'un 5,5% complété de l'information que l'on commence à 67%. Notez également que, ce chiffre se basant sur une étude unique, il est plus à considérer comme une approximation que comme une affirmation. D'autres études auraient trouvé d'autres chiffres, vraisemblablement proches mais pas forcément, selon le profil des personnes, selon le niveau de formation aux premiers secours dans le pays en question, selon les critères se constitution de la cohorte étudiée, *etc.*

Alors à partir de là, j’imagine que certains peuvent se dire que c’est idiot d’apprendre la version fausse en se disant que d’approcher de la vérité ne peut qu’apporter une meilleure compréhension du sujet. Et d’autres peuvent se dire que tout ça c’est de la branlette intellectuelle, et qu’on perd notre temps à se poser ce genre de questions. Peut-être qu’effectivement cette bataille de chiffre n’arrive pas à vous convaincre de l’utilité de ne pas chercher à tout prix la vérité absolue lorsque l’on transmet une information, mais je vais vous raconter une histoire un peu plus appliquée qui, j’espère, va d’avantage vous convaincre.

*****

### Le mieux, ennemi du bien

Cet exemple je ne vais pas le chercher bien loin. On va continuer de parler d’arrêt cardiaque et de secourisme. *Question :* faut-il accompagner un massage cardiaque d’un moyen de ventilation  comme le bouche-à-bouche par exemple ? *Réponse :* Bien sûr que oui ! En plus de faire circuler le sang, il est important de s’assurer que celui-ci est oxygéné. *Deuxième question* : faut-il apprendre au grand public à faire un bouche-à-bouche en plus du massage cardiaque ? Après avoir répondu *oui* à la première question difficile de ne pas répondre *oui* à la seconde… et pourtant…

On peut commencer par [cette revue](https://www.cochrane.org/CD010134/EMERG_continuous-chest-compression-versus-interrupted-chest-compression-cardiopulmonary-resuscitation-cpr), disponible sur le site de Cochrane, qui nous apprend que lorsque la réanimation est assurée par des personnes non professionnelles, les chances de survies sont en fait meilleures lorsque le massage cardiaque n’est pas accompagné de bouche-à-bouche.

[Une des études](https://www.nejm.org/doi/10.1056/NEJMoa0908993) retenues dans cette revue de la littérature va un peu plus loin et précise que la différence est plus nette encore lorsque la cause de l’arrêt cardiaque est… cardiaque… (Oui, parce qu’on peut aussi faire un arrêt cardiaque après avoir suffoqué, s’être noyé, *etc.*) Ici, si le cœur est la première chose à avoir lâché, il semble clairement plus efficace de ne demander au témoin non professionnel de ne faire que le massage cardiaque.

Avec ces informations, on commence à sentir que le bon comportement n’est pas le même pour tous les arrêts cardiaques. Dans [une autre étude](https://www.resuscitationjournal.com/article/S0300-9572(10)00953-6/fulltext), on s’intéresse cette-fois à distinguer les arrêts cardiaques pris en charge tardivement de ceux pris en charge tôt après la survenu de l’arrêt. Ce qui en ressort, c’est que les réanimations avec ou sans bouche-à-bouche ont des succès vaguement similaires si l’arrêt cardiaque date de moins de 15 minutes. En revanche, si la réanimation est entamée plus de 15 minutes après la survenue de l’arrêt, alors la réanimation avec bouche-à-bouche montre de meilleurs résultats. Encore une fois, on parle de prises en charges par des témoins non professionnels.

Là, on commence à avoir une meilleure compréhension qui se profile. Une explication crédible serait par exemple que l’on ait, au moment d’un arrêt cardiaque d’origine cardiaque, un sang suffisamment oxygéné pour que le bouche-à-bouche ne soit pas indispensable tout de suite. Il n’est pas impossible non plus, comme le note plusieurs de ces études, que le bouche-à-bouche soit trop complexe pour beaucoup de monde, qui vont hésiter, perdre du temps à le faire, ou mal le faire, et au final au lieu de perdre ce temps, il aurait mieux valu de continuer à faire tourner le sang.

Il y a un autre étage qu’il faut apporter à notre réflexion : le bouche-à-bouche est assez technique, et est parfois un repoussoir. En effet, il se peut que la personne qui vient de tomber au sol devant vous ne soit pas spécifiquement propre… il se peut que vous hésitiez à poser votre bouche sur la bouche d’un ou d'une inconnue… il se peut aussi que vous suspectiez la victime d’être atteinte d’une maladie transmissible. Bref ! Pour plein de raisons, bonnes ou mauvaises, les gens hésitent et parfois finissent par ne rien faire plutôt que de ne faire que la moitié de ce qu’on leur a appris en formation.

Et il se trouve qu’il y a maintenant plusieurs études (deux exemples [ici](https://www.ahajournals.org/doi/abs/10.1161/CIRCULATIONAHA.118.038179) et [là](https://jamanetwork.com/journals/jama/fullarticle/186668)) qui concluent que d’avoir abandonné le bouche-à-bouche dans leurs consignes a permis d’améliorer le taux de personnes qui survivent à un arrêt cardiaque. Pas nécessairement parce que c’était la bonne chose à faire en terme de geste technique, mais d’une part parce qu’au moins, les compressions thoraciques ne s’arrêtent jamais, et que d’autre part les témoins d’un arrêt cardiaque sont beaucoup plus nombreux à oser intervenir et porter secours lorsque ça n’implique pas de faire un bouche-à-bouche.

*****

### Conclusion

Que retenir de tout ça, donc ? Si vous avez envie de retenir qu’on ne perd pas 10% de chances de survie à la minute, mais \\(C(\\%) = 67 -2,3a - 1,1b -2,1c\\) (\\(a\\), \\(b\\), et \\(c\\) étant le temps passé sans massage cardiaque, sans défibrillation, et sans prise en charge complète, respectivement) ... libre à vous ! Vous mourrez moins bête. (*Mais vous mourrez quand même !*) Si vous souhaitez retenir que le bouche-à-bouche, c’est pour les cas où les secours n’arriveront pas tout de suite et dans les cas où l’arrêt cardiaque n’est pas initialement de cause cardiaque, pourquoi pas. Gardez en tête qu'on n'a pas abordé tous les cas de figures. Avec ce dont on vient de parler, vous êtes loin de tout savoir sur la prise en charge des arrêts cardiaques.

Ce que personnellement je vous invite à retenir, c’est que dans tous les cas, ***le massage cardiaque est d’une importance capitale***. Si vous hésitez, si vous ne vous rappelez plus trop bien comment faire un bouche-à-bouche, faites déjà un massage cardiaque, et ***ce sera très bien !*** Même fait approximativement, il est utile. ***Ne soyez pas bloqué parce que vous n’auriez pas de diplômes de secourisme.***

En termes de stratégie de santé publique, là où il y a le plus de marge de progression, ça n’est pas de fignoler les stats de survie en renforçant l’apprentissage d’un beau bouche-à-bouche bien propre et fonctionnel, c’est d’***augmenter drastiquement le nombre de témoins qui, même peu ou pas formés, osent faire le pas et font au minimum un massage cardiaque.*** Un bouche-à-bouche efficace en plus, c’est mieux, c’est vrai. Mais ce qui semble ressortir de ces études, c’est qu’il semble exister des cas de figures précis (ici, les instructions à donner à des témoins d’un arrêt cardiaque) où l’on a potentiellement plus à y gagner à simplifier le message qu’à enseigner au grand public des gestes et des règles de prise de décision trop complexes, aussi fidèles soient-elles à nos connaissances scientifiques.

*****

Voilà qui conclu ce billet, que j’avais eu envie de faire après avoir eu beaucoup de questions de la part d’apprenants secouristes en formation PSC1, qui me demandaient pourquoi on leur apprenait tel ou tel geste, alors qu’ils avaient lus en ligne ou appris que dans un autre pays ils font les choses différemment. Ce n’est pas que la physiologie des américains ou des suédois est différente de celle des français, ça n’est pas non plus que votre formateur vous aura menti en disant quelque chose de différent de ce que vous trouvez en ligne, c’est qu’on doit faire un arbitrage entre d’un côté ce qui serait le meilleur massage cardiaque théoriquement possible, et d’un autre côté les consignes qui permettent à maximum de témoins d’avoir confiance en ce qu’ils peuvent faire, et d’oser faire le pas de porter secours.

*****

*Post scriptum*, à l'attention des pompiers et associations se sécurité civiles, ne vous privez pas de réemployer les schémas de ce billet pour remplacer ou compléter l'immonde courbe pixellisée du référentiel PSE sur laquelle nous avons toutes et tous dû nous former.
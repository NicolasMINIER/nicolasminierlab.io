---
title: Tests génétiques - 1 - Introduction
description: "Premier billet de la série consacrée au génome et aux tests génétiques"
date: 2024-01-09T00:00:00.545Z
preview: ""
draft: false
tags: [Tests ADN]
categories: [Tests ADN]
---

Bienvenu dans ce premier billet de la série consacrée au séquençage ADN ! Avant d’aller explorer un génome (une partie du mien, pour illustration), quelques billets préliminaires me semblent nécessaires pour bien comprendre de quoi on parle. Ne vous en faites pas, ça n'aura pas la densité d'un cours de biologie, et consistera simplement en un florilège d'infos utiles pour comprendre de quoi il en retourne, et savoir ce que vous pouvez attendre et conclure des résultats que vous communiquent les labos d’analyse. Savoir repérer également ce qui ne sont que des éléments de pubs ou des titres *putaclic*, sans fondements scientifiques.

**********

## L'ADN, c’est grand comment ?
Une "brique" d’ADN, c’est petit, très petit ! Comptez environ 2 à 3 nanomètres de diamètre pour un double brun d'ADN, et un tiers de nanomètre de long pour chaque nucléotide (A, C, G, T) parcouru dans la longueur du brin. En revanche, un brin d’ADN complet, c’est grand, très grand ! Avec 47 millions de paires de bases (ou "pb", *i.e.* 2 nucléotides se faisant face dans la double hélice), le chromosome 21, qui est le plus petit chromosome humain mesure déjà 2,6 cm ! Avec 46 chromosomes, et 6,4 milliards de paires de bases, le génome humain complet mesurerait, si on l’étirait, un peu plus de deux mètres de long (\\(0,336 nm/pb \times 6,4.10^{9} pb = 2,15 m\\)), la taille exacte dépendant entre autres choses de la solution dans laquelle il baigne.

**********

## C’est où ?
L’ADN, ça se trouve dans le noyau de nos cellules… mais pas que ! Il y a une fraction de notre ADN que l’on a vite tendance à oublier, alors qu’il nous est vital : l’ADN mitochondrial. Celui-ci ne trouve pas dans le noyau, mais dans les mitochondries, de petits organites (comprenez des compartiments de la cellules) indispensables dans la production de molécules clés dans le fonctionnement énergétique de nos cellules. Dans ces mitochondries, on trouve de l’ADN. Pas beaucoup : 37 gènes tout juste, pour 16 kpb (milliers de pb), soit la moitié du cent-millième du génome complet. Mais ! Une mitochondrie se balade en général avec une petite dizaine de copie de ce génome, et une cellule peut avoir plusieurs milliers de mitochondries. Si on somme ces copies du génome, on passe à 5% du génome complet, ce qui est loin d'être négligeable.

**********

## Combien de gènes avons-nous ?
En réalité, lorsque l’on additionne les portions de l’ADN codantes pour des gènes, ainsi que toutes les régions périphériques participant de près ou de loin à la régulation de leur expression, on n’obtient que 38% de notre ADN. Puis si on ne s’arrête qu’aux gènes en eux-mêmes, nous ne regardons plus que 25% du génome. Pire encore, si on ne souhaite garder que la partie réellement codante de ces gènes (les "exons", dont la séquence dicte la composition des protéines) il ne nous reste qu’environ d’1,3% du génome. Mais bon ! 1,3% d’une si grande molécule, c’est suffisant pour coder pour tout un tas de gènes ! Initialement, on s’attendait à trouver entre 80 et 100 milliers de gènes. Rapidement après les premiers séquençages quasi-complets (en 2001), on a déchanté, et l’estimation est tombée autour de 30 à 40 milliers de gènes. Aujourd’hui (j'ai écrit ce script en 2016), on est plus proche de 20 à (peut-être) 25 milliers de gènes codant pour des protéines.

**********

## Est-ce vrai que nous sommes "tous les mêmes à 99,9%" ?
Vous l’avez sans doute déjà entendu, nous serions toutes et tous les mêmes, à 0,1% près. C'est notamment une annonce forte d'un [discours de Bill Clinton](https://www.youtube.com/watch?v=slRyGLmt3qc&t=518s) en juin 2000, alors que la recherche sur le génome humain livraient de premiers résultats préliminaires. Avant de commenter ce chiffre, il va falloir comprendre comment il est possible quantifier la différence entre deux séquences d'ADN. Pour ça, il faut savoir quels genres de différences il peut y avoir entre deux brins d’ADN.

Tout d’abord, il peut y avoir un changement dans la séquence (*e.g.* ACCGTATCAA devient ACGG**A**ATCAA). Facile à quantifier : nombre de changements (1) divisé par la taille de la séquence (10), et vous avez votre chiffre (10%).

Ensuite il peut y avoir ce qu’on appelle des "indels", qui sont des **in**sertions ou **dél**étions. Encore une fois, on peut se dire nombre de base supprimées/ajoutées, divisé par le nombre de base initial, et c’est bon. Mais parfois on ne sait pas qui était présent le premier. Que fait-on alors ? On compare à la moyenne ? On décide arbitrairement que l’une des deux séquences sera la séquence de référence ?

Egalement, on peut avoir des répétitions de séquences. Est-ce qu’on considère que le génome n’a pas changé ? Est-ce qu’on considère ça comme une insertion classique ? Et si une modification intervient, est-ce que l’on compte la modification une nouvelle fois ? Est-ce que notre façon de compter change selon que ce qui est dupliqué est codant ou non-codant ? Si on se met à produire 2x plus d’une protéine, est-ce que ça change notre façon de mesurer la "différence" entre deux individus ? Et si le changement intervient dans un endroit du génome sans aucune conséquence ?

Bref ! Normalement, vous commencez à comprendre où je veux en venir : les différences entre deux génomes, ça ne se quantifie pas très bien, et ça peut difficilement se résumer en un seul chiffre. Si vous voulez regarder les différences entre deux génomes, il va falloir qualifier chaque différence pour comprendre de quoi il en retourne.

Pour autant, il faut savoir que, une fois qu’on considère les substitutions et les courts indels, on a pris en compte l'écrasante majorité des différences entre les génomes. Donc ne compatibiliser que les substitutions, et diviser ça par la taille moyenne d’un génome, on ne fait pas une erreur si grande que ça. Ça ne donne aucune indication sur les différences en question, mais pour un discours politique, on va pas chipoter !

**********

## Du coup, Clinton a menti ?
En 2001, le *Human Genome Project* annonce avoir fini de séquencer le génome humain. Et parmi tous les chiffres qu’ils ont donnés pour résumer leurs résultats, il y a celui-ci : ils ont identifié environ 1,4 million de SNP (prononcer "snip"). Un SNP, c’est simplement une modification d’un seul nucléotide (*Single Nucleotide Polymorphism*, en anglais). Sur cette base, ils ont estimé qu’il devait y avoir au total un SNP toutes les 1000 pdb, soit une conservation du génome pour 99,9% de sa séquence (en ne comptant que les SNP comme différence possible entre deux génomes). Donc Clinton ne mentait pas ! C’est une grosse approximation, qui cache beaucoup de choses derrière (comme expliqué plus haut), mais encore une fois, c’est dans un discours politique, pas un congrès scientifique.

Mais ça, c’était en 2001. On avait vaguement séquencé nos premiers génomes (à 94% environ), et avec si peu de personnes séquencées (quelques unes), on ne pouvait de toute façon voir que ce qui était très fréquent, et pas les modifications plus rares. Aujourd’hui, on a séquencé des milliers de génomes à peu près complètement, et on arrive à décerner des modifications beaucoup plus rares. De 1,4M de SNP connus, on en dénombre aujourd’hui (2015) 40M. Donc même en faisant la même approximation que précédemment, on arrive à 1,3% du génome qui est variable… Et ce chiffre risque de grimper encore à l’avenir… Et on ne parle ici que des SNP, qui ne sont qu'une des différences que l'on peut trouver entre deux génomes.

Je dois ajouter cependant, après avoir disucter avec des ami·es et collègues biologistes, qu'il y a une autre façon d'interpréter la phrase "nous sommes tou·tes à 99,9% les mêmes" : en tirant au sort deux être humains quelque soit leur origine, on trouvera en moyenne des différences sur environ 0,1% de leurs génomes (soit quelque chose de proche du premier chiffre avancé par le *Human Genome Project*, du temps où seuls quelques individus avaient vu leur génome séquencé). Il me semble assez clair (mais je peux me tromper) que ce n'était pas dans ce sens qu'étaient prononcés les mots de Clinton, mais il faut noter qu'on peut retomber sur nos pattes en 2016 en intéprétant la phrase ainsi.

Tout ça pour dire qu’il faut se méfier des résultats qui cherchent à résumer de nombreuses réalités différentes derrière un seul et même chiffre. En revanche, ce n'est sans doute pas la peine de se battre obstinément à rectifier le chiffre de 99,9% entendus partout depuis 2001 : la principale chose à retenir, c'est que quantifier la différence entre deux individus est une gageure (et c'était déjà vrai en 2001 !).

**********

## Et le séquençage, dans tous ça ?
Alors pourquoi est-ce que je vous parle de tout ça dans cette introduction ? L’objectif de ce billet était de vous faire passer deux idées : Premièrement, les séquences d’ADN codantes pour des protéines ne constituent qu’une petite partie de l’ADN. Ensuite, les séquences d’ADN de deux individus ne diffèrent pas tant que ça entre elles. Pour une bonne partie, ces différences prennent la forme de substitutions d’une base par une autre, à des endroits bien précis et dont nous connaissons la localisation pour beaucoup d'entre eux. Si ces endroits sont au milieu d’exons (la partie codante d’un gène), il se peut que la fonction de ce gène soit affectée, ou pas ! Si on se trouve autour de ces gènes, dans des régions régulatrices par exemple, on va pouvoir avoir un impact sur l’expression de ce gène, qui pourrait être surexprimé, ou sous-exprimé… ou pas ! Et si on se trouve ailleurs, il y a surtout des chances qu’il ne se passe rien du tout. Si la modification touche un gène important, cela risque de déboucher sur une pathologie. Sinon, vous risquez de changer de couleur de peau, de cheveux, d’yeux, *etc.* Et où que l’on soit, ce sont des modifications héréditaires, et constituent des outils idéaux pour étudier les généalogies humaines.

C’est exactement cela que font les principaux labos d’analyse de génome que vous trouverez sur internet. Pas besoin de séquencer un génome complet pour en apprendre plus sur vous, on en apprend déjà énormément en se focalisant sur ces quelques % du génome dont on sait qu’ils sont susceptibles de changer. Par exemple, le labo auquel j’ai confié un échantillon en 2016 testait alors un demi-million de SNP. Mais ça, on en parlera une prochaine fois !

**********

#### Sources
[Genomes. 2nd edition. Brown TA. Oxford: Wiley-Liss; 2002.](https://www.ncbi.nlm.nih.gov/books/NBK21128/)

[Molecular Biology of the Cell, 4th edition. Bruce Alberts, Alexander Johnson, Julian Lewis, Martin Raff, Keith Roberts, and Peter Walter. New York: Garland Science; 2002.](https://www.ncbi.nlm.nih.gov/books/NBK21054/)

[Sachidanandam R, Weissman D, Schmidt S. et al. The International SNP Map Working Group. A map of human genome sequence variation containing 1.42 million single nucleotide polymorphisms. Nature. (2001);409:928–933.](https://doi.org/10.1038/35057149)

[Venter et al., 2001. The sequence of the human genome. Science.](https://doi.org/10.1126/science.1058040)

[Initial sequencing and analysis of the human genome. International Human Genome Sequencing Consortium. Nature 409, 860–921 (15 February 2001)](https://www.nature.com/articles/35057062)

[A global reference for human genetic variation. The 1000 Genomes Project Consortium. Nature 526, 68–74 (01 October 2015)](https://www.nature.com/articles/nature15393)

[An integrated map of genetic variation from 1,092 human genomes. The 1000 Genomes Project Consortium. Nature 491, 56–65 (01 November 2012)](https://www.nature.com/articles/nature11632)

[Mandelkern et al., 1981. The dimensions of DNA in solution.](https://doi.org/10.1016/0022-2836(81)90099-1)

*********

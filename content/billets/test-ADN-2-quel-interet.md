---
title: Tests génétiques - 2 - Quel intérêt ?
description: ""
date: 2024-01-10T00:00:00.545Z
preview: ""
draft: false
tags: [Tests ADN]
categories: [Tests ADN]
---

#### Non, vous n'avez aucune chance d'échapper à votre génome
*Et autres informations utiles sur l'intérêt d'étudier un génome*

NB : ceci est l'adaptation à l'écrit de l'épisode [ADN 103](https://www.youtube.com/watch?v=yf4P74FjkvY) de la série couvrant les tests génétiques. Il fait suite au billet d'[introduction](/billets/test-adn-1-introduction/), et précède celui concernant la [transmission](/billets/test-adn-3-transmission/).

**********

Vous le savez déjà, votre groupe sanguin dans le système ABO ne vient pas de nulle part. Il s’agit d’un trait héréditaire, qui vous a été légué par vos parents biologiques, et qui peut se lire directement dans votre génome. Depuis quelques décennies, maintenant que l’exploration de l’ADN est entamée, les biologistes repoussent toujours plus loin la limite du libre arbitre et la longue molécule semble ne plus laisser de marge de manœuvre à quoi que ce soit d’autre qu'une grande et implacable équation bio-chimique. Alors certes, il semble de plus en plus clair que pas un seul évènement de nos vies ne se produise sans la participation, à un moment ou à un autre, de nos gènes… mais on va tout de même pouvoir formuler quelques objections à la toute-puissance de nos gènes !

**********

**Non, vous n’avez aucune chance d’échapper à vos gènes, mais** il ne faut pas se tromper quant à la nature de leur impact. Il n’existe pas, par exemple, un gène des études, qui vous pousserait, contre vents et marées et quel que soit votre environnement social, à vous consacrer à vos études et à cumuler les diplômes. Pas plus qu’il n’existe de gène qui vous destinerait à devenir pompier, ou maçon, ou secrétaire. Nous ne sommes pas dans une tragédie grecque, nos gènes ne sont pas la pythie, et si nous avions eu accès au génome d’Œdipe, nous n’y aurions jamais lu qu’il tuerait son père et épouserait sa mère.

Notre génome ne donne pas un cap à suivre ni un objectif qu’il nous pousserait à atteindre. Si l’on devait résumer ce que fait notre génome en un seul concept, c’est d’orchestrer la chimie de nos organismes. L’impact d’une modification du génome se ressentira au niveau moléculaire. Vous pourrez avoir un peu plus ou un peu moins d’une certaine hormone, par exemple. Ensuite, et ça n’est qu’une conséquence, alors peut-être que celle balance hormonale vous rendra plus ou moins sensible que la moyenne au stress, ou au vertige, ou à l’alcool, ou que sais-je ! Ce sont des éléments qui vont participer à qui vous êtes et comment vous vous construisez mais ce n’est pas un chemin tout tracé, et il ne décide certainement pas d’un objectif à atteindre.

**********

**Non, vous n’avez aucune chance d’échapper à votre génome, mais** il n’est pas la seule influence sur votre vie ! Et selon les cas, le génome d’une personne pourra être le principal coupable, tout comme parfois il ne jouera qu’un rôle mineur. Reprenons d’un côté l’exemple des groupes sanguin dans le système ABO. Si vous avez des allèles (c’est-à-dire des versions d’un gène) qui code pour A, ce sera votre groupe, quasiment à coup sûr. Même si ça serait théoriquement possible, ce serait extrêmement improbable que vos gènes disent A, et qu’il en soit autrement dans les faits. D’un autre côté, il est facile d'imaginer des exemples où vos gènes ne joueront qu’un rôle mineur. Imaginons par exemple que deux personnes chutent d'un immeuble de 10 étages. Il y a toute une variété de gènes qui vont être impliqués dans la densité de leurs ossatures respectives, dans leurs réflexes et donc indirectement dans leur capacité à agir durant la chute et dans quelle position ils vont tomber, mais ce ne sera sans doute pas assez pour contrecarrer ce qui aura eu le plus d’impact : l'environnement et non le génome. Même si deux personnes aux génomes complétement différents pourraient ne pas finir avec exactement les mêmes fractures, le résultat (la mort) changera peu, et vous n’auriez obtenu aucune espèce d'information à séquencer le génome des deux personnes.

La plupart des questions que l'on se pose spontanément, lorsque l'on envisage l'influence de notre génome sur notre vie, se trouvent entre ces deux extrêmes. Connaître son génome n'apporte de (quasi-)certitudes que si l'on se limite à l'échelle moléculaire. Au délà de ça, le génome n'apporte rapidement que des éléments de réponses très parcélaires, voire futiles comparé à l'impact de l'environnement.

**********

**Non, vous n’avez aucune chance d’échapper à vos gènes, mais** on ne sait pas tous les lire non plus ! Une protéine que notre organisme produit et nécessairement codée par un ou plusieurs gènes. Dans le cas de l’insuline par exemple, un seul gène suffit à lui donner sa séquence. Et même si d’autres gènes sont impliqués dans la production d’enzymes et autres molécules qui vont interagir avec l’insuline pour lui donner une forme mature, définitive, on peut tout de même être sûr que, si vous n’avez pas ce gène ou qu’il ne marche pas, vous ne produirez pas d’insuline. À l’échelle de la production d’une molécule, c’est assez facile d’avoir un degré de certitude proche de 100%. Mais si l’on cherche à savoir si une personne va développer un Alzheimer en vieillissant, ça devient beaucoup plus compliqué. Aussitôt que de nombreux gènes sont impliqués et qu’on ne les connait pas forcément tous, on est bien en difficulté pour tirer des conclusions, quand bien même on aurait la certitude que la pathologie en question est d'origine uniquement génétique. Et quand c’est le cas, on est obligé de répondre en statistiques. Ainsi, la réponse d’un généticien pourra être de vous dire *« dans la population générale, il y a 1,5 personnes sur 100000 à être porteuses de cette maladie. Vous, avec votre génome, on vous place dans un groupe où la prévalence est plutôt autour de 1,3/100000 »* Ce qui n’est pas forcément la réponse attendue par quelqu’un qui a payé pour faire séquencer son génome et qui entend à la télé depuis qu’il est petit que la génétique est toute puissante.

**********

**Non, vous n’avez aucune chance d’échapper à vos gènes, mais** vos gènes ne contiennent pas d’informations que sur votre avenir. En effet, même si apprendre de nombreux détails sur votre biologie peut vous aider à mieux cerner le chemin que vous parcourez de votre vivant, tout dans votre génome n’est pas là pour séquencer des protéines. Il y a de nombreuses régions que l’on a un peu facilement tendance à juger inutiles, où une mutation n’aura pour ainsi dire aucun impact. Ces mutations, qui peuvent être transmises de génération en génération, sont extrêmement intéressantes pour étudier les migrations humaines. Ainsi, si une mutation précise est fréquemment retrouvée dans une zone géographique donnée, mais également plus dispersée ailleurs, ce sera un indice fort laissant supposer qu’elle est apparue là, et que des gens vivants là à l’époque ont migré dans telle ou telle direction. La génétique des vivants est ainsi une mine d’or d’informations sur notre passé collectif.

**********

Voilà donc 4 points pour nuancer le discours d’un asservissement complet des individus à leur patrimoine génétique. Certes, nos gènes sont impliqués dans absolument tout ce qui se produit dans notre vie, mais… il ne faut pas croire que notre génome est un plan de vol dessinant comment notre vie se déroulera. Notamment parce qu’il arrive nos gènes ne joue qu’un rôle marginal voir quasi non-existant dans les évènements que nous vivons. Ensuite, ce rôle (qu'il soit marginal ou conséquent), on ne sait pas toujours le déchiffrer, tant les gènes sont nombreux, et leurs intrications complexes. Enfin, parce qu’il est parfois plus juste de voir notre génome comme un héritage du passé que comme une prophétie sur l’avenir.

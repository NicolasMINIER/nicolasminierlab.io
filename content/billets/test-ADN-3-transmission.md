---
title: Tests génétiques - 3 - Transmission
description: ""
date: 2024-01-11T00:00:00.545Z
preview: ""
draft: false
tags: [Tests ADN]
categories: [Tests ADN]
---

#### Non, vous n'avez pas les chromosomes de vos parents
*et autres infos sur la transmission de l'ADN*

NB : ceci est l'adaptation à l'écrit de l'épisode [ADN 104](https://www.youtube.com/watch?v=QsVzQ9VdYB4) de la série couvrant les tests génétiques. Il fait suite au billet portant sur l'[intérêt](/billets/test-adn-2-quel-interet/) de séquencer un génome, et précède celui concernant les [questions auxquelles répond un test génétique](/billets/test-adn-4-quelles-questions/).

**********

Depuis quelques billets, on parle de gènes et de leur hérédité, et il est grand temps que l’on parle de la modalité de cette hérédité ! Alors comment les gènes se transmettent de générations en générations ? Vous allez voir, ça va être l’occasion de revenir sur quelques idées reçues.

**********

S’il y a qqc de sûr, en biologie, c’est que c’est le bordel ! Alors pour ne pas perdre tout le monde, on simplifie, parfois à outrance. Un exemple, et qui va nous intéresser aujourd’hui, c’est l’intégrité des chromosomes. Quand on pense à un chromosome, on pense souvent à l'image ci-dessous (gauche) : une grosse molécule empaquetée, qui forme un bloc. Durant la fécondation, on apprend tous que l’on reçoit un chromosome de notre père et un de notre mère, pour chacune des 23 paires de chromosomes. Et tout ça donne l’impression que ces chromosomes forment des blocs unis, et qu’un même chromosome va donc être transmis de génération en génération. Et cette idée reçue est si tenace qu’elle se retrouve même sur le site internet du labo qui à qui j’ai donné un échantillon à séquencer (dont est issu l'illustration ci-dessous).

![transmission-chromosomes-cliche](/billets/tests-ADN/23andMe-illustration.png "Illustration de la transmission des chromosomes que l'on peut retrouver sur le site de 23&Me")

Alors les chromosomes restent-ils réellement inchangés au fil des générations ? Avez-vous vraiment des chromosomes en commun avec vos parents ? La réponse, vous la voyez venir : **non**. Et le principal coupable dans cette affaire, s’appelle *crossover*, et désigne la tendance qu’ont parfois les chromosomes à s’enjamber les uns les autres jusqu’à s’échanger des bouts de leur séquence. Dans la plupart des cas, il s’agit de deux chromosomes appariés (désignés par le même numéro, *e.g.* deux chromosomes "12"), et qui vont s’échanger des séquences situées aux mêmes endroit dans leurs séquences respectives. Ce n’est pas très embêtant, dans la mesure où les deux chromosomes à la fin de l’opération portent les mêmes gènes en même quantité. De manière moins courante, les morceaux échangés peuvent ne pas se valoir, et un chromosome finira avec plusieurs exemplaires d’un même gène tandis que l’autre n’en aura plus aucun exemplaire. Autre possibilité peu fréquente, s’échanger des bouts entre chromosomes différents. Ces phénomènes de *crossover* peuvent se produire à chaque division cellulaire, et sont surtout connus pour être quasiment systématiques lors de la méiose, c’est-à-dire les divisions cellulaires qui vont produire spermatozoïdes et ovules. Ainsi, même si vous avez bien reçu 23 chromosomes de votre père et 23 de votre mère (en règle générale, et c'est sans aborder la question de l'ADN mitochondrial), ces chromosomes que l’on vous a légué ne sont en général pas identiques à ceux de vos parents. Ils sont composés *à partir* de ceux de vos parents, mais ne sont pas *ceux* de vos parents.

![differents-crossovers](/billets/tests-ADN/Crossover_global.png "Illustration du principe d'un crossover")

{{< gallery caption-effect="fade">}}
  {{< figure link="/billets/tests-ADN/Crossover-homologue.png" caption="Crossover homologue">}}
  {{< figure link="/billets/tests-ADN/Crossover-non-homologue.png" caption="Crossover non-homologue">}}
  {{< figure link="/billets/tests-ADN/Crossover-translocation.png" caption="Translocation">}}
{{< /gallery >}}

Mais alors qu’est-ce que ça change ? Tout simplement le nombre de nos ancêtres qui nous auront réellement légué un bout de leur génome. Si les chromosomes étaient des entités immuables, avec 23 paires de chromosomes, on pourrait au mieux retracer l’histoire de 46 de nos ancêtres à chaque génération donnée. Mais en réalité, notre génome est le résultat de la participation de centaines voire de milliers d’ancêtres, chacun ayant participé pour une petite portion.

Mais alors, dans tout de fatras, n’y a-t-il rien de stable ? Eh bien si ! Il y a dans le génome de chacun, 1 ou 2 éléments relativement stables.

Parlons d’abord du chromosome Y. Pour la plupart d’entre nous, nous avons soit 2 chromosomes *X*, soit 1 *X* et 1 *Y*. Ce sont les deux cas les plus courants, mais pas les seuls. Toujours est-il qu’il est très rare qu’un chromosome Y ne soit pas seul. Du coup, les possibilités de recombinaison sont limitées, et le chromosome Y d’un fils est quasi systématiquement le même que celui de son père, et de son père avant lui, et ainsi de suite. On peut ainsi distinguer des groupes d’individus, ayant un ancêtre paternel direct en commun. C’est ce qu’on appelle un "haplogroupe" (paternel, dans notre cas). Les chromosomes X se retrouvant parfois à plusieurs (notamment chez la grande majorité des femmes), les recombinaisons ne sont pas si rares que ça, et on perd vite la trace de leur origine.

En revanche, il y a un autre élément relativement stable dans nos génomes, et que cette fois nous avons tous : les mitochondries. En effet, même si l’on retrouve de ces petits organites dans les spermatozoïdes comme dans les ovules, ceux apportés par le spermatozoïde sont détruits très tôt après la fécondation, dans l'immense majorité des cas. On ne retrouve donc en général que les mitochondries de la mère. Qui du coup sont celle de la grand-mère, vous avez compris le principe. On peut donc identifier des haplogroupes maternels également.

![haplogroupes](/billets/tests-ADN/haplogroupes.png)

Illustration de la transmission de chromosomes Y au sein de deux haplogroupes (rouge et bleu). Tous les hommes (carrés en couleur sombre) n'ont qu'un seul chromosomes Y, hérité de leur père. Eux commes leurs soeurs appartiennent au même haplogroupe (rouge ou bleu). Cependant, leurs soeurs (ronds en couleur plus claire) n'ont pas de chromosome Y, et doivent se référer au génome d'un autre membre de son haplogroupe pour connaître l'histoire génétique de la lignée patri-linéaire de sa famille.
{.legend}

Résumons ! Il y a dans votre arbre généalogique, 1 ou 2 éléments de votre génome dont on peut retracer l’histoire. Il s’agit du génome mitochondrial, passé de mère à enfants et qui nous classe tous dans des haplogroupes maternels, ainsi que du chromosome Y, qui lui ne se passe que de père en fils et permet d'identifier des haplogroupes paternels. Mais contrairement à ce que l’on pourrait penser, le reste de notre génome n’est pas si stables que ça, et votre arbre généalogique ne correspond pas à une grande course de relais où les chromosomes se passent de génération en génération. Il faudrait donc plutôt le voir comme une fresque collective à laquelle ont participé de très nombreux et nombreuses inconnues. Et le principal responsable de tout ce bordel, vous le connaissez maintenant, il s’agit du *crossover*.

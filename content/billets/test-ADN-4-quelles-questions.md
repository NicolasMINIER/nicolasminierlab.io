---
title: Tests génétiques - 4 - Quelles réponses ?
description: ""
date: 2024-01-12T00:00:00.545Z
preview: ""
draft: false
tags: [Tests ADN]
categories: [Tests ADN]
---

#### Non, un test génétique ne répondra pas à vos questions
*ou du moins, pas à celles que l'on se pose spontanément*

NB : ceci est l'adaptation à l'écrit de l'épisode [ADN 105](https://www.youtube.com/watch?v=leuW1497rHA) de la série couvrant les tests génétiques. Il fait suite au billet portant sur la [transmission](/billets/test-adn-3-transmission/) de séquencer un génome, et précède le premier billet concernant les résultats, et notamment l'[origine géographique](/billets/test-adn-5-resultats-origines/).

**********

***Ai-je un ancêtre viking ? Aurai-je Alzheimer plus tard ? Suis-je Français de souche ?*** Si vous avez fait séquencer votre génome dans l’espoir de répondre à une de ces questions, ou une similaire dans sa tournure, j’ai une mauvaise nouvelle pour vous : vous risquez d’être déçu·e ! Bien qu’un séquençage vous apporte des éléments de réponse, ce n’est pas exactement à ces questions-là qu’il s’adresse. Prenons quelques minutes pour comprendre sur quoi votre ADN peut vous renseigner précisément, et quelles sont les limites aux réponses apportées.

**********

#### « Ai-je un ancêtre viking ? »
On se rêve tous descendant d’un peuple dont on se fait une idée un peu plus épique, un peu plus romancée. On peut idéaliser l’ordre romain, la civilisation grecque, les explorateurs vikings, ou les artisans celtes. Secrètement, on aimerait pouvoir se dire *« voilà mon héritage »*. Et pour s’en convaincre on est prêt à lâcher quelques billets dans l’espoir que notre génome réponde à notre attente. Et pourtant, je vois au moins trois raisons qui pourraient vous faire passer à côté de l’information que vous cherchiez.

Premièrement, les résultats d’un test ADN vous renseigneront sur le lieu supposé de l’apparition d’une mutation, autrement dit : « où vivait la personne dont l’ADN a muté ? ». Or le lieu de survenue d’une mutation et le peuple auquel cette personne appartenait ne se superposent pas nécessairement. Prenons nos fiers vikings. Si l’ADN de l’un d’entre eux mute peu de temps après qu’il parte avec toute sa famille dans une expédition commerciale et ne s’implante sur les rives de la Volga, et bien tout laissera penser que cette mutation est typiquement d’Europe de l’Est plutôt que du Nord. Ce ne sera QUE si nous disposons d’autres éléments que l’on pourra rectifier le tir et vous annoncer la véritable histoire de votre ADN. Avoir un ancêtre qui était un marchand parti explorer de nouvelles contrées serait un élément de votre histoire que vous aimeriez sans doute apprendre, mais s’il était un étranger au milieu d’un autre peuple, on passera vraisemblablement à côté de l'info.

Deuxièmement, la plupart des mutations exploitées par les tests ADN s’étant produites il y a déjà bien longtemps, et les humains ayant la fâcheuse tendance à copuler partout où ils passent, on les retrouve souvent sur un large territoire, parfois sur un continent entier ou plus. Et les entreprises, lorsqu’elles vous donnent la provenance géographique de votre ADN en pourcentages, ne vous donnent en réalité qu’une approximation, parfois grossière. Elles ne peuvent, en réalité qu’essayer de construire une version crédible des faits. Supposons qu’une mutation, un SNP, soit retrouvée sur tout un territoire en faible proportion, et en forte proportion en un point bien identifié de celui-ci. La concentration en cet endroit est-elle due au fait qu’il s’agisse du point d’apparition de la mutation ? Ou bien cette mutation est-elle apparue il y a longtemps ailleurs sur ce territoire, et la population vivant là où la concentration est la aujourd'hui plus forte aura-t-elle plus résisté que d’autre au brassage génétique, du fait qu’elle soit une communauté fermée par exemple, ou qu’elle ne se soit pas soumise à un envahisseur ? Encore une fois, on vous donnera comme réponse ce qui semble le plus probable, et ce ne sera pas nécessairement une version des faits en laquelle on a une confiance absolue. D’ailleurs vos résultats changeront dans le temps, en fonction de nouvelles découvertes et alors que de plus en plus de monde font séquencer une partie de leur génome.

![snp-heatmap](/billets/tests-ADN/snp-heatmap.png)

Enfin, tous nos ancêtres n’ont pas contribué à notre génome. A priori, ni votre mère, ni votre père ne vous a légué 100% de son génome. De fait, après quelques générations, il est tout à fait possible qu’un de vos ancêtres soit sorti du tableau. Il ou elle peut très bien être votre ancêtre biologique, et pourtant ne pas faire partie de votre "arbre généalogique génétique". Encore une fois, il se peut très bien qu’un grand Roi fasse parti de votre arbre généalogique, et que pour autant 100% de votre ADN provienne de paysans. Votre génétique ne couvrira qu’une fraction de vos ancêtres. Il ne suffit pas qu’un illustre personnage fasse effectivement parti de votre arbre généalogique pour qu’un test ADN puisse le confirmer ou l’infirmer.

![arbre](/billets/tests-ADN/arbre-genealogique-genetique.png)

S'il est fort probable que chacun de nos grands-parents nous ai légué une portion de leur génome, sitôt que l'on remonte plus loin dans notre arbre généalogique, beaucoup de monde ne nous on pas laissé d'héritage génétique. Ainsi, la présence d'un illustre personnage dans notre arbre généalogique a de très fortes chances de n'avoir aucune espèce d'importance en ce qui concerne notre génome.
{.legend}

**********

#### « Aurai-je Alzheimer plus tard ? »
Voilà une deuxième question qui peut peut-être vous motiver à acheter un kit ADN. Lire notre destin biologique et s’y préparer peut effectivement paraître tentant. Malheureusement, encore une fois, un test ADN ne connait pas la réponse à cette question. En revanche, il peut répondre à une question relativement proche : suis-je plus ou moins à risque que la population générale de développer telle ou telle pathologie ? En effet plutôt que de considérer la population générale chez qui la une maladie aurait une chance X de se manifester, on peut vous classer dans une catégorie de la population ayant des génomes proches, et chez qui la maladie a une chance Y de se déclarer. Vous pourrez alors comparer X et Y. J’en reste là pour aujourd’hui, mais vous verrez dans le prochain billet qu’il est en réalité facile de mal interpréter ces résultats et de finir hypochondriaque !

**********

#### « Suis-je Français de souche ? »
C’est un usage fréquent des test ADN : se rattacher à une ethnie, apporter la preuve que l’on est un "sang pur", [parfois pour tomber sur des surprises](https://www.youtube.com/watch?v=ptSZnTtGCQA). Pour répondre à cette question, il va d’abord falloir définir ce que vous entendez par « de souche ». Si vous vouliez fournir la preuve que vous appartenez à telle ou telle "race", laissez tomber de suite, ce n’est pas un sujet de biologie. Si vous vouliez prouver que 100% de vos ancêtres proviennent d’une même aire géographique, vous ferez face à deux obstacles de taille : premièrement, l’être humain n’est pas apparu en France (pour prendre cet exemple), et beaucoup des mutations qui caractérisent les génomes français sont retrouvés un peu partout en Europe de l’Ouest et au delà. Et si vous êtes tentés de reformuler la question en « Suis-je issu de français depuis au moins 2000 ans » vous devrez rapidement rendre les armes, car 2000 ans, ce n’est pas énorme pour une espèce animale comme la notre, il n’y a pas beaucoup de matière à exploiter. Pas suffisamment de mutations spécifiquement françaises ne sont apparues pour qu’on puisse donner un score absolu. Un génome soi-disant « français » sera en réalité un mélange plus ou moins précis de pas mal de choses qui circulent en Europe et même un peu plus loin en Asie et de l’autre côté de la méditerranée, en Afrique. Vous n’aurez donc jamais une certitude à 100% que votre génome soit français depuis aussi longtemps qu’il puisse possiblement l’être. Enfin, rappelez vous ce que l’on disait quelques minutes plus tôt, tous vos ancêtres n’ont pas participé à votre génome, et un test ADN ne concernera donc qu’une partie de votre arbre généalogique, et ne couvrira pas tous vos ancêtres.

Formulée différemment, il y a une question à laquelle on pourra répondre avec un test ADN : à quel point mon génome est-il commun ? À quel point mon génome ressemble-t-il à celui des autres ?

Vous ne pourrez donc pas savoir si vous êtes plus français qu’un autre, mais vous pourrez savoir si votre génome est banal parmi les génomes des autres français. On reviendra dans le prochain billet sur comment vous pouvez le faire.

**********

Voilà qui termine les quelques billets d’introduction aux test ADN. Vous devriez normalement commencer à saisir, avec ces trois exemples, qu’il peut y avoir de grosses différences entre les questions que l’on se pose soi-même en achetant un de ces kits, et les questions qui vont réellement trouver réponse grâce à notre génome.

**Ne vous posez donc pas la question de savoir si vous avez des ancêtres vikings**, celtes, ou phéniciens, mais attendez-vous à en apprendre plus sur le lieu d’apparition des mutations que vous portez. Que ce soit il y a quelques siècles à deux pas de chez vous, ou en Afrique il y a 100 milles ans. Avec des estimations à la louche de l’époque d’apparition de la mutation, ainsi que de la composition de la population à cette époque et en ce lieu, on peut tenter de dresser une hypothèse des peuples porteurs de cette mutation, mais pas de certitudes absolues.

**Ne cherchez plus à savoir si vous aurez Alzheimer plus tard**, mais peut-être apprendrez-vous que vous appartenez à une catégorie de la population plus ou moins à risque que les autres.

**Ne vous attendez pas à prouver que vous êtes Français(e) de souche**, vous saurez tout au plus si votre génome est banal parmi telle ou telle population actuelle.

Comme promis, la prochaine fois que l’on se voit sur cette série consacrée à l’ADN, ce sera pour enfin commencer à décortiquer les résultats !

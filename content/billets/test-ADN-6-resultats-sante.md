---
title: Tests génétiques - 6 - Résultats 2/2 - Santé
description: ""
date: 2024-01-09T00:00:00.545Z
preview: ""
draft: true
tags: [Tests ADN]
categories: [Tests ADN]
---

NB : ceci est l'adaptation à l'écrit de l'épisode [ADN 107](https://www.youtube.com/watch?v=uQ1umireny0) de la série couvrant les tests génétiques. Il fait suite au billet traitant du [volet "origines" des résultats](/billets/test-adn-5-resultats-origines/), et clôture cette série.

**********

“Article 16-10 du code civil
L'examen des caractéristiques génétiques d'une personne ne peut être entrepris qu'à des fins médicales ou de recherche scientifique.
Le consentement exprès de la personne doit être recueilli par écrit préalablement à la réalisation de l'examen, après qu'elle a été dûment informée de sa nature et de sa finalité. Le consentement mentionne la finalité de l'examen. Il est révocable sans forme et à tout moment. »
Article 226-28-1 du code pénal => Peine = 3750€ d’amende

« Le fait, pour une personne, de solliciter l'examen de ses caractéristiques génétiques ou de celles d'un tiers ou l'identification d'une personne par ses empreintes génétiques en dehors des conditions prévues par la loi est puni de 3 750 € d'amende. »

Article 226-26
« Le fait de détourner de leurs finalités médicales ou de recherche scientifique les informations recueillies sur une personne au moyen de l'examen de ses caractéristiques génétiques est puni d'un an d'emprisonnement et de 15 000 euros d'amende. »

https://www.legifrance.gouv.fr/affichCode.do;jsessionid=74C274D7ED337C5B965574AC52675572.tplgfr26s_2?idSectionTA=LEGISCTA000006165397&cidTexte=LEGITEXT000006070719&dateTexte=20190202

Bonjour.

GENERIQUE

Dans la précédente vidéo, nous avions parlé de ce qu’un test ADN tel qu’il s’en vend sur internet actuellement, peut vous apprendre sur vos origines. Aujourd’hui est venu le temps d’aborder la question de la santé ! C’est de loin un sujet beaucoup plus sensible, et la loi française est beaucoup plus protectrice/contraignante (selon comment vous voyez la chose) que dans d’autres pays comme au hasard les Etats Unis d’Amérique.

Alors passons en revue dans un premier temps ce qu’on peut extraire comme information sur la santé d’une personne à partir de son ADN, et dans un second temps ce qui est légal ou non, en France notamment.

Le génome étant une sorte de plan d’architecte ou d’ingénieur pour construire notre corps, on peut effectivement y trouver les détails de notre métabolisme, ainsi que nos défauts de conceptions. On peut facilement savoir si on a une version fonctionnelle de tel ou tel gène, qui peut par exemple nous aider à digérer certains aliments, ou à nous défendre contre un pathogène. Une fois que vous avez la séquence de votre génome entre les mains, vous pouvez très facilement savoir si vous êtes intolérant au lactose, porteur d’une mutation qui est associée à un métabolisme de l’alcool plus rapide, ou d’une résistance plus forte au VIH, par exemple.

Alors une fois qu’on a dit ça, on est en droit de se demander « mais qui est le con qui a décidé que ça allait être interdit en France de proposer aux gens de séquencer leur génome ? » Quand on voit la mine d’or d’information sur sa propre santé que cela représente, pourquoi nous empêcher d’y avoir accès ? Nan mais c’est vrai, ce que cette loi dit, philosophiquement parlant, c’est qu’il y a des informations sur vous-même et votre santé, des infos qui devraient faire partit de votre dossier médical, qu’il est illégal de chercher à savoir. Il y a des choses sur vous-même que vous n’avez pas le droit de savoir, sauf exception. C’est quand même particulier.

Et pourtant, vous allez voir, ces lois n’existent pas sans raisons, et je vais en détailler deux ici.

Premièrement, et c’est quelque chose qu’il faut garder en tête, votre génome vous appartient-il vraiment ? À mois d’avoir un vrai jumeau, il a fort à parier que vous soyez le seul être humain qui n’ait jamais porté et portera jamais ce génome précis. Et encore, même dans le cas des vrais jumeaux, on n’est pas à l’abri de quelques changements au cours de votre vie. Donc c’est le vôtre et celui de personne d’autre, nan ?
Et pourtant non, car même si votre génome vous est propre, il est généralement hérité de vos parents, la plupart du temps à hauteur de 50% papa 50% maman, plus ou moins quelques fractions de pourcents, voire quelques pourcents selon les cas, et selon la méthode de calcul.
Et du coup, on peut voir un génome non pas comme un identifiant ou un pseudonyme qui ne représenterait que vous et vous-même, mais plutôt comme un prénom propre à un individu, mais également associé à un nom de famille. Si par exemple vous vous appelez Erik Ragnarson et que vous faites de la merde, vous ne le faite pas qu’au nom d’Erik, mais aussi au nom de Ragnar. Et bah le génome c’est pareil. Que vous le vouliez ou non, si on vous identifie, on a déjà fait la moitié du boulot pour identifier vos parents et vos frères et sœurs et vos enfants, et une partie du boulot pour identifier tous les autres membres de votre famille.

Et là on se trouve dans une situation très délicate, où pour disposer de votre génome comme bon vous semble, il vous faudrait logiquement avoir l’accord de tous les membres de votre famille, sur plusieurs générations, et jusqu’à ce qu’un généticien ne puisse plus vous relier à quiconque. Pire, il faudrait que tous vos proches ne se fasse jamais séquencer, sinon on finirait par pouvoir vous identifier de proches et proches. Et comme il n’est pas tout à fait impossible qu’il y ait des membres de votre famille biologique que vous ne connaissiez pas, on peut décemment considérer que vous n’arriverez pas à garantir que vous avez l’accord de tout le monde pour user de ce génome qui est à la fois personnel et collectif.
Vous le sentez sans doute venir depuis quelques secondes, un cas particulier que la loi cherche à protéger est celui des donneurs de gamètes. En effet, si on imagine qu’une personne fasse un don de gamètes, et que son frère ou sa sœur passe un test génétique en ligne, et que ce soit également le cas d’une personne née de ce don de gamète, et bien l’identité du donneur est dévoilée sans qu’il n’ait jamais donné son avis à aucun moment !
Et c’est pas cool. Du tout.	

Bref, vous devriez commencer à comprendre que votre génome n’appartient pas vraiment qu’à vous, et que c’est un casse-tête légal de chercher à savoir ce qu’il est souhaitable que chacun puisse faire ou non avec le sien, puisqu’il le partage de fait avec d’autres. Faire cohabiter des tests ADN pour se trouver de la famille d’un côté, et l’anonymat des donneurs de gamètes d’autre part, est impossible. On ne peut en garder qu’un sans le compromettre. La loi française fait le choix de préserver l’anonymat des donneurs, et c’est un choix qui se justifie.

Bon, la recherche de liens de parentés c’est compliqué, mais si on se limitait au versant santé, du coup ?  Qu’est-ce qui nous empêche d’y jeter un œil ?

Alors effectivement sur le côté santé, on peut se dire que toute information est bonne à prendre. Alors certes, mais pas n’importe comment.

En France, le principe de base, c’est qu’une information sur votre état de santé, un diagnostic, doit être délivré en tête-à-tête avec un professionnel de santé. Alors il y a des exceptions, et je pense que la plus connue ce doit être les tests de grossesses, qui permettent de faire des autodiagnostics, sans avoir de professionnel de santé pour nous l’annoncer. Mais la règle générale, de base (Article L1111-2 CSP), c’est que ça doit se faire de manière individuelle, avec un professionnel de santé, c’est-à-dire un ou une médecin, infirmière, kiné, dentiste, sage-femme, etc. mais sûrement pas un site internet étranger sans aucun interlocuteur.

Sauf que quand vous trouvez sur un même site internet d’une part que telle version d’un gène signifie avoir telle maladie quand telle autre signifie ne pas être malade, et d’autre part que votre ADN est sous telle forme, ce serait quand même insulter l’intelligence de tout le monde que de prétendre ne pas avoir posé de diagnostic !
C’est pour cette raison que les sites qui vous vendent des tests ADN se garderont bien de vous donner quoi que ce soit d’autre que les résultats bruts. Ils se permettent de révéler des infos sur l’origine géographique, et ça a l’air d’être toléré par la justice, qui ne se saisie pas du truc.

Quoi qu’il en soit, si vous voulez en savoir plus sur vos prédispositions génétiques en matière de santé, il faudra donc passer par d’autres sites, qui ne vous annonceront rien, mais ne feront que commenter des versions de gènes. Après, si vous décider de renseigner précisément vos allèles, ça, ça ne les regarde pas, vous faites ce que vous voulez. Voilà le genre de slalom qu’il faut faire pour vous renseigner. Ça reste relativement facile, pour qui veut le faire.

De manière générale, le cadre légal est assez flou. Dans leur récapitulatifs sur les examens génétiques, les états généraux de la bioéthique décrivent ces tests ADN comme un défi fait à la loi française. L’agence de biomédecine nous informe que les colis pour test ADN peuvent être saisis par la douane et donner lieu à 1 an de prison + 15 000€ d’amende, mais on va pas se mentir, si la douane ne repère pas ces colis-là, c’est qu’ils ne sont pas en train de les chercher très activement. Alors est-ce qu’il faut considérer que c’est un contournement de la loi française, une faille dans un ensemble de lois trop vieilles et pas adaptées, ou est-ce que aussitôt que l’expédition de matériel génétique se fait depuis la France, il faut considérer que ce test est fait en France ? L’esprit des lois que j’ai citées plus haut est de protéger contre une atteinte faite à une personne. Le cas de l’exploration de son propre génome n’était pas spécialement prévu par nos lois, et je pense qu’il n’y a qu’un juge qui pourra trancher la question, si un procès l’amène à devoir interpréter nos lois. À ma connaissance, ça ne s’est pas encore produit, mais on peut tout à fait imaginer que cela arrive dans le futur. Pour reprendre le cas des donneurs de gamètes, il est largement possible qu’un ou une donneuse voit son identité révélée contre sa volonté, et qu’il ou elle décide de porter plainte.

Bref ! Omettons quelques secondes l’aspect légal, et considérons que vous avez décidez d’explorer votre génome tout seul comme un grand, parce que de toute façon, y’a pas vraiment de risque de se tromper en lisant ça, nan ? Une mutation, on l’a ou on l’a pas, nan ?

Techniquement, oui, mais on va prendre quelques exemples, et vous allez vous rendre compte que de savoir ça ne permet pas de si facilement tout comprendre du premier coup.

1er exemple : vous renseignez votre génome dans un de ces sites qui permettent de commenter les mutations présentes sur un génome, et entre deux infos sur le métabolisme de la caféine et votre propension à la dépendance alcoolique (#levinrougeESTunalcoolcommeunautre), vous tombez nez à nez avec le chiffre suivant : +90% de chance de développer une maladie mortelle X avant l’âge de 40ans, comparé à la population générale. Donc là vous respirez un grand coup, vous prenez le premier crayon à votre portée, et vous commencez à rédiger un testament.
Sauf que non ! Alors oui +90% de développer telle ou telle maladie pas cool, ça fait peur, mais là, en vrai, cette information seule est pas si effrayante que ça ! Typiquement, si l’incidence de la maladie dans la population générale est de 1 cas pour 1 000 000, alors dans le groupe des gens qui ont cette mutation, l’incidence est elle de 1,9 pour 1 000 000. En somme vos chances de développer la maladie sont passées de « EXTREMEMENT improbables » à « extrêmement improbables ». Vous pouvez reposer votre crayon.

Bon vous vous dites sans doute que vous ne tomberez pas dans le piège, mais ce n’est pas tout. 2e exemple : vous continuez d’explorer les résultats, et vous lisez
Cancer du côlon : +20%
Cancer du pancréas : +5%
Cancer du sein : +30%
Cancer de la peau : +3%

Et ça continue, encore et encore, sur des lignes et des lignes… Bon là vous reprenez votre crayon et sortez une nouvelle feuille.
Sauf qu’encore une fois, non ! Ces quelques lignes seules ne suffisent pas à constituer une alerte. Des mutations qui ont un impact sur la prolifération cellulaire et sur la survenue de cancers, il y en a un paquet. Vraiment. Des milliers. Autant vous prévenir tout de suite, si vous vous lancer dans l’analyse de votre génome, vous y trouverez des milliers de mauvaises nouvelles, ainsi que des milliers de bonnes nouvelles, et tomber sur une suite de chiffres qui font peur, ça vous arrivera plus d’une fois. Et même si vous n’êtes pas hypochondriaque, je vous assure que vous fera un petit effet. Pour savoir si vous avez plus de ces mauvaises nouvelles que de bonnes, il vous faudrait avoir en tête les dizaines de milliers de SNPs prisent en compte par le logiciel, et comparer ça à ce qu’on les gens en moyenne. (very bad trip + explosion)

Parce que oui, 3e exemple, c’est bien beau d’avoir un chiffre qui fait peur, ou un grand nombre de mutations délétères, mais dans l’absolu, le site web où vous êtes vous comparera à un génome de référence. Or ce génome de référence n’est pas nécessairement représentatif de la population dans laquelle vous évoluez. Vous pouvez par exemple avoir une mutation pour laquelle il est annoncé 2x plus de cas d’une maladie qui n’est déjà pas si rare que ça dans notre population, et vous vous dites qu’il faut absolument voir un spécialiste immédiatement pour être suivi, alors que cette mutation est présente dans votre pays chez 90% des gens. Donc en fait la « mauvaise nouvelle », c’est que vous êtes un patient comme un autre, ceux que les médecins ont l’habitude de suivre tous les jours, et que donc il n’y a pas besoin d’une attention particulière. Vous êtes un humain normal, vous pouvez souffler.

Bref, je vais pas me lancer dans une liste exhaustive de tous les pièges, qu’ils soient biologiques, statistiques ou cliniques dans lesquels on peut tomber en analysant son propre ADN tout seul, mais j’aimerais terminer sur un dernier : le faire soi-même sur internet, c’est le meilleur moyen de finir par poser ses questions sur internet dans la foulée, ce qui assez souvent est une erreur, bien que ces derniers temps, les référencements google ont tendance à s’assainir. Si vous traînez sur les réseaux sociaux francophones, il y a de fortes chances pour que, de temps en temps, vous voyez sur vos fils d’actualité des posts venant des pages santé/bien-être de journaux parfois connus, voire de magazines qui se disent spécialisés. Soyons honnêtes, c’est de la merde. Dans le meilleur des cas, il vous faudra repérer les 1 ou 2 journaliste compétent en matière de santé-médecine, et dans le pire des cas il vaut mieux ne jamais avoir lu le moindre article si vous tenez à votre santé.

Alors que retenir de tout ça ? Premièrement, que séquencer son génome n’est pas encadré par la loi pour rien. Il y a un certain nombre de questions médicales, éthiques, et sociétales qui se posent, et justifient qu’on n’ouvre pas grand les vannes. Un test ADN est avant tout un gadget à 100€ pour voir apparaître sur une carte vos pays d’origines. Si vous activez cette option, c’est également un outil pour se chercher de la famille, qui se fait en mettant en danger l’anonymat des donneurs de gamètes. Enfin, c’est une mine d’or d’informations utiles, mais qui sont noyées dans un océan d’informations anxiogènes et difficilement interprétables si vous n’êtes pas compétent en génétique. Des mutations qui présentent un intérêt clinique, c’est-à-dire qui vont changer la façon dont votre médecin va s’occuper de vous, il y en a, mais elles ne sont pas légion, et il faut savoir les repérer et les analyser. Ça ne s’improvise pas en ligne.
Donc si j’avais un conseil à faire passer à ceux qui ont envie de passer un test ADN, c’est de ne pas le faire, ou du moins pas tout de suite. Pour plusieurs raisons : premièrement, le prix de ces gadgets va continuer de baisser, donc ne vous sentez pas pressés. Deuxièmement, on est actuellement en train de revoir nos lois de bioéthique, et la question de ces tests ADN fera l’objet d’évolutions dans la loi. Plutôt que de jouer le contournement des lois, patientez le temps que le cadre se clarifie. Troisièmement, c’est un outil dont l’apparition a pris de court un peu tout le monde. Il est même possible que votre médecin généraliste ne soit pas super préparé à répondre à ce genre de question. Des médecins agréés par l’agence de biomédecine pour accompagner les patients passant des tests génétiques, il y en a, mais ils ne sont pas encore très nombreux. Donc laissez le temps à notre système de santé d’intégrer ces nouveaux outils, vous serez bien mieux pris en charge dans quelques années que livrés à vous-même aujourd’hui à slalomer entre plusieurs sites internet pas toujours hyper sérieux.
Et si jamais c’est trop tard, que vous l’avez déjà fait, et qu’il y a telle ou telle mutation qui vous fait peur, allez voir votre médecin, discutez avec lui de ces résultats et de vos antécédents familiaux. Sérieusement. Vous venez de dépenser 100€ pour un test ADN, alors s’il vous plaît même si ça vous coûte un peu de temps et d’argent, vos questions, adressez-les à un médecin. Pas à des arnaqueurs en ligne, ni à des homéo-naturo-gemmo-charlatan-thérapeutes. Prenez votre santé au sérieux, et soin de vous. C’est tout pour aujourd’hui.

https://etatsgenerauxdelabioethique.fr/pages/examens-genetiques-et-medecine-genomique

https://www.agence-biomedecine.fr/agrement-praticiens-genetique

https://www.agence-biomedecine.fr/IMG/pdf/20160915_encadrementinternational_actualisation2016_v4.pdf

https://www.has-sante.fr/portail/upload/docs/application/pdf/2013-02/regles_de_bonne_pratique_en_genetique_constitutionnelle_a_des_fins_medicales.pdf

---
title: Mùn
description: ""
date: 2023-11-25T15:06:20.545Z
preview: ""
draft: false
tags: []
categories: []
---

Cette page d'accueil compile quelques généralités sur le monde de Mùn, et renvoie vers les dernières évolutions de cette section du site.

## Généralités sur Mùn

Mùn est une île continent, seule terre émergée d'un monde façonné par les divinités. Habitée par des humains, elfes, nains, halflins et autres gnomes, elle sert de décors à mon imagination, et aux aventures de quelques un·es de mes collègues et ami·es.

![carte-du-monde](/donjons-et-dragons/mun/geographie/mun/mun-carte-sans-noms.png)

Son Histoire se découpe en 4 Âges majeurs : Âge des Elfes, Âge des Epées, Âge des Grimoires, et l'Âge des Prières, que vous trouverez détaillés plus amplement plus tard (quand j'en trouverai le temps).
---
title: Mémos
description: "Vulgarisation en biologie et santé publique"
date: 2023-11-25T00:00:00.545Z
preview: ""
draft: false
tags: []
categories: []
---

Vous trouverez ici quelques billets à usage personnel, pour coucher quelque part un résumé de lectures et visionnages qui m'ont intéressé.
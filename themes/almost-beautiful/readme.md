This theme, coined "Almost Beautiful", is an adaptation of the [Beautiful Hugo theme](https://themes.gohugo.io/themes/beautifulhugo/)

## License

MIT Licensed, see [LICENSE](https://gitlab.com/NicolasMINIER/nicolasminier.gitlab.io/-/tree/master/themes/almost-beautiful/LICENSE).
